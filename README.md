# WARC

**[Castellano](README.es.md)**

A [WARC](https://wiki.archiveteam.org/index.php/The_WARC_Ecosystem)
shard.

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     warc:
       git: https://0xacab.org/pip/warc.cr.git
   ```

2. Run `shards install`

## Usage

```crystal
require "warc/reader"

Warc::Reader.new("file.warc.gz").process do |record, payload|
  puts record["WARC-Type"]
end
```

## warc2html

Converts a WARCZ file to a static HTML site so it can be browsable
without extra steps on the user side.

It'll mangle the URLs so everything can be accessed using a web server
(ie. query strings are part of the path). It adds WARC metadata to HTML
pages so you can check.

### Installation

1. Download the "Static Linux binary" from the [latest
release](https://0xacab.org/pip/warc.cr/-/releases).

2. Extract the contents from the tar.gz file.

3. Move to this directory (folder) and run the `warc2html` command with
   the .warc.gz file as an argument:

   ```sh
   cd warc2html-*
   ./warc2html file.warc.gz
   ```

4. If there are errors they'll be shown while it's working.  Please
   report any errors on the [issue
   tracker](https://0xacab.org/pip/warc.cr/-/issues) or to
   `@archivo-pirata-antifascista@archivo-pirata-antifascista.partidopirata.com.ar`
   on the Fediverse.

5. To show what it's doing, prepend `LOG_LEVEL=DEBUG` to the command:

   ```sh
   LOG_LEVEL=DEBUG ./warc2html file.warc.gz
   ```

6. Files will the store under a `_site` directory.
