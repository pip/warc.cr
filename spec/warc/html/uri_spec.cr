require "csv"
require "../../spec_helper"
require "../../../src/warc/html/uri"

describe Warc::HTML::URI do
  describe "#convert!" do
    it "converts uris to localhost" do
      uri = URI.parse "https://cientificasquecuentan.mincyt.gob.ar/"
      html_uri = Warc::HTML::URI.new(uri)

      html_uri.convert!.should(eq(nil))
      html_uri.uri.to_s.should(eq("https://localhost/cientificasquecuentan.mincyt.gob.ar/index.html"))
    end
  end

  describe "#canonicalize_scheme!" do
    it "always sets https" do
      %w[
        cientificasquecuentan.mincyt.gob.ar/
        http://cientificasquecuentan.mincyt.gob.ar/
      ].each do |url|
        uri = URI.parse url
        html_uri = Warc::HTML::URI.new(uri)

        html_uri.canonicalize_scheme!.should(eq(nil))
        html_uri.uri.scheme.should(eq("https"))
      end
    end
  end

  describe "#absolute_path!" do
    it "makes urls absolute" do
      uri = URI.parse "index"
      html_uri = Warc::HTML::URI.new(uri)

      html_uri.absolute_path!.should(eq(nil))
      html_uri.uri.path.should(eq("/index"))
    end

    it "ignores already absolute paths" do
      uri = URI.parse "/index"
      html_uri = Warc::HTML::URI.new(uri)

      html_uri.absolute_path!.should(eq(nil))
      html_uri.uri.path.should(eq("/index"))
    end
  end

  describe "#add_hostname_to_path!" do
    it "moves the hostname to the path" do
      uri = URI.parse "https://cientificasquecuentan.mincyt.gob.ar/"
      html_uri = Warc::HTML::URI.new(uri)

      html_uri.add_hostname_to_path!.should(eq(nil))
      html_uri.uri.path.should(eq("/cientificasquecuentan.mincyt.gob.ar/"))
      html_uri.uri.hostname.should(eq("localhost"))
    end
  end

  describe "#convert_query_string_to_path!" do
    it "converts query strings to path elements" do
      uri = URI.parse "https://cientificasquecuentan.mincyt.gob.ar/?id=1"
      html_uri = Warc::HTML::URI.new(uri)

      html_uri.convert_query_string_to_path!.should(eq(nil))
      html_uri.uri.query.should(eq(nil))
      html_uri.uri.path.should(eq("//id/1/"))
    end

    it "decodes the query string" do
      uri = URI.parse "https://cientificasquecuentan.mincyt.gob.ar/index.php?rest_route=%2Foembed%2F1.0%2Fembed&url=https%3A%2F%2Fcientificasquecuentan.mincyt.gob.ar%2F%3Fpage_id%3D53"
      html_uri = Warc::HTML::URI.new(uri)

      html_uri.convert_query_string_to_path!.should(eq(nil))
      html_uri.uri.query.should(eq(nil))
      html_uri.uri.path.should(eq("/index.php/rest_route//oembed/1.0/embed/url/https://cientificasquecuentan.mincyt.gob.ar//page_id/53/"))
    end
  end

  describe "#add_trailing_slash!" do
    it "adds a trailing slash if missing" do
      uri = URI.parse "https://localhost/index"
      html_uri = Warc::HTML::URI.new(uri)

      html_uri.add_trailing_slash!.should(eq(nil))
      html_uri.uri.path.should(eq("/index/"))
    end
  end

  describe "#php_and_asp_are_always_dirs!" do
    it "changes extensions to html" do
      uri = URI.parse "https://localhost/index.php"
      html_uri = Warc::HTML::URI.new(uri)

      html_uri.extname.should(eq(".php"))
      html_uri.php_and_asp_are_always_dirs!.should(eq(nil))
      html_uri.extname.should(eq(".html"))
      html_uri.uri.path.should(eq("/index.html"))
    end

    it "removes the extname from path if query string is present" do
      uri = URI.parse "https://localhost/index.php?hey"
      html_uri = Warc::HTML::URI.new(uri)

      html_uri.extname.should(eq(".php"))
      html_uri.php_and_asp_are_always_dirs!.should(eq(nil))
      html_uri.extname.empty?.should(eq(true))
      html_uri.uri.path.should(eq("/index/"))
    end
  end

  describe "#add_index_if_trailing_slash!" do
    it "adds index" do
      uri = URI.parse "https://localhost/"
      html_uri = Warc::HTML::URI.new(uri)

      html_uri.add_index_if_trailing_slash!.should(eq(nil))
      html_uri.uri.path.should(eq("/index"))
    end
  end

  describe "#add_html_if_extname_empty!" do
    it "add the html extension if the extname is missing" do
      uri = URI.parse "https://localhost/index/"
      html_uri = Warc::HTML::URI.new(uri)

      html_uri.add_html_if_extname_empty!.should(eq(nil))
      html_uri.uri.path.should(eq("/index/.html"))
    end
  end

  it "converts a lot of urls" do
    CSV.each_row(File.open("spec/fixtures/urls.csv", "r")) do |(original_url, path)|
      uri = URI.parse original_url
      html_uri = Warc::HTML::URI.new(uri)

      html_uri.convert!.should(eq(nil))
      html_uri.uri.path.should(eq(path))
    end
  end
end
