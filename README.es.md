# WARC

**[English](README.md)**

Una _shard_ para [Crystal](https://crystal-lang.org/) que procesa
archivos en formato
[WARC](https://wiki.archiveteam.org/index.php/The_WARC_Ecosystem).

## Instalación

1. Agregar la dependencia al archivo `shard.yml`:

   ```yaml
   dependencies:
     warc:
       git: https://0xacab.org/pip/warc.cr.git
   ```

2. Correr `shards install`

## Uso

```crystal
require "warc/reader"

Warc::Reader.new("file.warc.gz").process do |record, payload|
  puts record["WARC-Type"]
end
```

## warc2html

Convierte un archivo WARCZ en un sitio estático que puede ser navegado
sin pasos extra para le usuarie.

Va a modificar las URLs para que funcione con un servidor web (por
ej. la _query_, lo que viene después del "?" en la URL, va a pasar
a formar parte de la ubicación del archivo).  Además agregar metadatos
desde el WARC a los archivos HTML para poder verificar.

### Instalación

1. Descarga el "Static Linux binary" desde la [última versión](https://0xacab.org/pip/warc.cr/-/releases).

2. Extrae los contenidos del archivo .tar.gz.

3. Ubicate en el directorio (carpeta) extraído y ejecuta el comando
   `warc2html` con el archivo .warc.gz como argumento:

   ```sh
   cd warc2html-*
   ./warc2html file.warc.gz
   ```

4. Si hay errores van a ser mostrados mientras el programa funciona.
   Por favor reporta cualquier error en el
   [repositorio](https://0xacab.org/pip/warc.cr/-/issues) o a
   `@archivo-pirata-antifascista@archivo-pirata-antifascista.partidopirata.com.ar`
   en el Fediverso.

5. Para poder ver lo que está haciendo, agrega `LOG_LEVEL=DEBUG` antes
   del comando:

   ```sh
   LOG_LEVEL=DEBUG ./warc2html file.warc.gz
   ```

6. Los archivos van a ser guardados en el directorio`_site`.
