require "./warc/reader"
require "./warc/html/uri"
require "log"
require "uri"
require "file_utils"
require "lexbor"
require "json"

Log.setup_from_env

file = ARGV[0]
uri_to_uri  = Hash(String, URI).new
uri_to_file = Hash(String, String).new
# @todo convertir a tabla sqlite3
attr_to_uri = Hash(String, URI).new
attributes = %w[src href]
copy_meta = %w[WARC-Record-ID WARC-Target-URI WARC-Date]
EXTENSIONS_WITH_QUERY = %w[.html .php .asp]

# Normaliza la URL
# @todo usar el content type informado para ajustar la extensión, porque
# a veces (moodle) los js y css se sirven vía php
def fix_path(uri : URI, store) : URI
  store[uri.to_s] ||= Warc::HTML::URI.new(uri).tap(&.convert!).uri
end

Warc::Reader.new(file).process do |record, payload|
  next unless record["WARC-Type"] == "response"
  next unless payload.is_a? HTTP::Client::Response
  next unless payload.success?

  uri = URI.parse(URI.decode_www_form(record["WARC-Target-URI"]))
  hostname = uri.hostname.to_s
  uri = fix_path(uri, attr_to_uri)

  next if hostname.empty?

  path = "./_site#{uri.path}"

  Log.debug { path }

  next if File.exists? path

  FileUtils.mkdir_p File.dirname(path)
  is_html = payload.headers.includes_word?("Content-Type", "text/html")
  html = Lexbor::Parser.new("")

  if is_html
    html = Lexbor::Parser.new(payload.body_io.gets_to_end)

    copy_meta.each do |meta|
      meta_element = html.create_node(:meta)
      meta_element["name"] = meta
      meta_element["content"] = record[meta]
      html.css("head").first.append_child(meta_element)
    end

    attributes.each do |attribute|
      html.css("[#{attribute}]").each do |element|
        if element.tag_name == "link" && element["rel"]?
          skip = false

          %w[preconnect dns-prefetch].each do |rel|
            skip = skip || (element["rel"] == rel)
          end

          next if skip
        end

        value = fix_path(URI.parse(URI.decode_www_form(element[attribute])).tap do |v|
          v.host ||= hostname
          v.path   = "/" if v.path.empty?
        end, attr_to_uri)

        element[attribute] = uri.relativize(value).to_s

        # @todo process srcset
        element["srcset"] = nil
      end
    end

    # @todo css @import
    # @todo js frameworks
  end

  File.open(path, "w") do |file|
    if is_html
      file.write html.to_html.to_slice
    else
      begin
        IO.copy payload.body_io, file
      rescue e : IO::Error
        Log.warn &.emit("Failed to write body", error: e.message.to_s, uri: record["WARC-Target-URI"])
      ensure
        payload.body_io.close
      end
    end
  end
end
