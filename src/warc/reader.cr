require "log"
require "compress/gzip"
require "http/client/response"
require "http/request"

module Warc
  alias Record = Hash(String, String)

  class Reader
    getter file : String

    CRLF = "\r\n"
    WARC_LINE = "WARC/1.0\r\n"

    def initialize(@file)
    end

    # Processes each WARC record and yields the block to it to perform
    # transformations.
    #
    # The WARC file is read line by line and processed.  The block runs
    # just before the next record.
    def process(&block)
      # In which part of the process we are
      step : Symbol = :start
      # Cache two previous lines
      previous_line : String? = nil
      previous_previous_line : String? = nil
      # The WARC record
      record : Record = Record.new
      # HTTP lines
      # @todo Stream directly to the parser
      http = [] of String

      # Open the file
      File.open(file) do |f|
        # Decompress it
        Compress::Gzip::Reader.open(f) do |gzip|
          # Read line by line without removing CRLF
          gzip.each_line(false) do |line|
            # Start a new WARC record
            if warc_start?(line)
              # Process the previous HTTP payload
              unless http.empty?
                reader, writer = IO.pipe
                payload : HTTP::Client::Response | HTTP::Request | HTTP::Status | Record | Nil

                # Since we parse from IO, we need to write lines to a
                # pipe
                spawn do
                  begin
                    http.each do |l|
                      writer.write l.to_slice
                    end
                  rescue e : IO::Error
                    Log.warn &.emit("Failed to write payload", error: e.message.to_s, uri: record["WARC-Target-URI"])
                  # Don't forget to close!
                  ensure
                    writer.close
                  end
                end

                # Parse the payload according to the WARC type
                #
                # @todo Support more types
                case record["WARC-Type"]
                when "response"
                  begin
                    HTTP::Client::Response.from_io?(reader) do |response|
                      payload = response
                    end
                  rescue e : IO::Error
                    Log.warn &.emit("Failed to parse response payload", error: e.message.to_s, uri: record["WARC-Target-URI"])
                    payload = nil
                  end
                when "request"
                  begin
                    payload = HTTP::Request.from_io(reader)
                  rescue e : IO::Error
                    Log.warn &.emit("Failed to parse request payload", error: e.message.to_s, uri: record["WARC-Target-URI"])
                    payload = nil
                  end
                when "warcinfo"
                  payload = Record.new
                  previous_key : String? = nil

                  reader.each_line do |line|
                    # Do nothing, probably end of body
                    if line.empty?
                    # Concatenate with the previous key
                    elsif previous_key && line.starts_with?(" ")
                      payload[previous_key] = "#{payload[previous_key]}#{line.lstrip}"
                    # Parse header and store the key for next line
                    else
                      key, value = line.split(": ", 2)
                      payload[key] = value
                      previous_key = key
                    end
                  end
                end
              end

              # Yield
              yield(record, payload) unless step == :start

              # Start again
              record = Record.new
              http = [] of String
              line = nil

              # Signal we need to look for WARC headers
              step = :warc
            # Signal we're looking for an HTTP payload
            elsif step == :warc && line == CRLF
              step = :http_start
            end

            case step
            when :warc
              # Just parse the headers
              #
              # @todo DRY
              unless line.nil?
                key, value = line.split(": ", 2)
                record[key] = value.strip
              end
            # Just skip the line since it's a CRLF
            when :http_start
              step = :http
            # Start collecting the HTTP payload
            when :http
              # The record ends with a triple CRLF
              if previous_previous_line == CRLF && previous_line == CRLF && line == CRLF
              else
                # Collect lines for later
                http << line unless line.nil?
              end

              # Store for next run
              previous_previous_line = previous_line
              previous_line = line
            end
          end
        end
      end
    end

    # Detects if we're at the start of a WARC record
    def warc_start?(line)
      line == WARC_LINE
    end
  end
end
