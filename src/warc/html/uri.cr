require "uri"

module Warc
  module HTML
    class URI
      getter original_uri : ::URI
      getter uri : ::URI
      getter extname : String
      getter html : Bool

      EXTENSIONS_WITH_QUERY = %w[.html .php .asp]
      ALWAYS_DIRS = %w[.php .asp]
      CHARS_TO_PATH = %w[& = ? ! @]

      def initialize(@original_uri)
        @uri = original_uri.dup
        @extname = File.extname(uri.path)
        @html = EXTENSIONS_WITH_QUERY.includes?(extname) || uri.path.ends_with?("/") || extname.empty?
      end

      def html?
        html
      end

      def query_string?
        !uri.query.to_s.empty?
      end

      def convert!
        canonicalize_scheme!
        normalize_port!
        absolute_path!
        add_hostname_to_path!
        php_and_asp_are_always_dirs!
        convert_query_string_to_path!
        add_index_if_trailing_slash!
        add_html_if_extname_empty!
        squeeze!
      end

      def canonicalize_scheme!
        uri.scheme = "https"

        nil
      end

      def normalize_port!
        uri.port = nil

        nil
      end

      def absolute_path!
        uri.path = "/#{uri.path}" unless uri.path.starts_with?("/")

        nil
      end

      def add_hostname_to_path!
        uri.path = "/#{uri.host}#{uri.path}"
        uri.host = "localhost"

        nil
      end

      def convert_query_string_to_path!
        return unless html?
        return unless query_string?

        uri.query = ::URI.decode_www_form(uri.query.to_s)

        replace_chars_in_query_for_path!

        uri.path = "#{uri.path}/#{uri.query}"
        uri.query = nil

        add_trailing_slash!

        nil
      end

      def add_trailing_slash!
        uri.path = "#{uri.path}/" unless uri.path.ends_with?("/")

        nil
      end

      def php_and_asp_are_always_dirs!
        return unless ALWAYS_DIRS.includes? extname

        if query_string?
          uri.path = uri.path.sub(extname, "")
          @extname = ""

          add_trailing_slash!
        else
          uri.path = uri.path.sub(extname, ".html")
          @extname = ".html"
        end

        nil
      end

      def add_index_if_trailing_slash!
        uri.path = "#{uri.path}index" if uri.path.ends_with?("/")

        nil
      end

      def add_html_if_extname_empty!
        uri.path = "#{uri.path}.html" if html? && extname.empty?

        nil
      end

      def squeeze!
        uri.path = uri.path.squeeze("/")

        nil
      end

      def replace_chars_in_query_for_path!
        CHARS_TO_PATH.each do |char|
          uri.query = uri.query.to_s.tr(char, "/")
        end
      end
    end
  end
end
